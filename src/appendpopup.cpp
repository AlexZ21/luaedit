#include "appendpopup.h"
#include "codeedit.h"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QListView>
#include <QLineEdit>
#include <QSortFilterProxyModel>
#include <QStandardItemModel>
#include <QDir>
#include <QFile>

#include <QDebug>

USING_NS_LE

AppendPopup::AppendPopup(const QString &scriptsDirPath, QWidget *parent) :
    QFrame(parent),
    m_scriptsDirPath(scriptsDirPath),
    m_scriptsListFilter(new QLineEdit(this)),
    m_scriptsListView(new QListView(this)),
    m_codePreview(new CodeEdit(this))
{
    QHBoxLayout *mainLayout = new QHBoxLayout();
    mainLayout->setMargin(2);
    mainLayout->setSpacing(2);
    setLayout(mainLayout);

    QVBoxLayout *leftSideLayout = new QVBoxLayout();
    leftSideLayout->setSpacing(2);
    mainLayout->addLayout(leftSideLayout);

    m_scriptsListFilter->setFixedWidth(150);
    leftSideLayout->addWidget(m_scriptsListFilter);

    m_scriptsModel = new QStandardItemModel();
    m_scriptsFilterModel = new QSortFilterProxyModel();
    m_scriptsFilterModel->setSourceModel(m_scriptsModel);
    m_scriptsListView->setModel(m_scriptsFilterModel);
    m_scriptsListView->setFixedWidth(150);
    m_scriptsListView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    leftSideLayout->addWidget(m_scriptsListView);

    QVBoxLayout *rightSideLayout = new QVBoxLayout();
    rightSideLayout->setSpacing(2);
    mainLayout->addLayout(rightSideLayout);

    m_codePreview->setReadOnly(true);
    rightSideLayout->addWidget(m_codePreview);

    loadScriptsList();

    connect(m_scriptsListFilter, &QLineEdit::textEdited,
            [this](const QString &text){
        QRegExp regExp("*" + text + "*", Qt::CaseInsensitive, QRegExp::Wildcard);
        m_scriptsFilterModel->setFilterRegExp(regExp);
    });

    connect(m_scriptsListView, &QListView::doubleClicked,
            [this](const QModelIndex &index){
        emit selected(index.data(Qt::UserRole + 100).toString());
    });

    connect(m_scriptsListView->selectionModel(), &QItemSelectionModel::currentChanged,
            [this](const QModelIndex &current, const QModelIndex &previous){
        Q_UNUSED(previous)
        if (current.isValid())
            m_codePreview->setPlainText(current.data(Qt::UserRole + 100).toString());

    });

    setFrameShape(QFrame::StyledPanel);
    setAttribute(Qt::WA_DeleteOnClose, true);
    setWindowFlags(Qt::Popup);
}

void AppendPopup::show(const QPoint &point)
{
    move(point);
    QWidget::show();
}

void AppendPopup::loadScriptsList()
{
    m_scriptsFilterModel->setFilterRegExp(QString());
    m_scriptsModel->clear();

    //Загрузка списка скриптов
    QDir dir(m_scriptsDirPath);
    if (dir.exists()) {
        QStringList scriptPaths = dir.entryList({"*.lua"});

        for (const QString &script : scriptPaths) {
            QFile scriptFile(m_scriptsDirPath + "/" + script);
            if (scriptFile.open(QIODevice::ReadOnly)) {

                QString fileData = scriptFile.readAll();

                int titleSection = fileData.indexOf("[title]");
                if (titleSection == -1)
                    continue;

                fileData.remove(0, titleSection + QString("[title]").length());

                int sourceSection = fileData.indexOf("[source]");
                if (sourceSection == -1)
                    continue;

                QString scriptTitle;
                QString scriptSource;

                scriptTitle = fileData.mid(0, sourceSection).trimmed();

                fileData.remove(0, sourceSection + QString("[source]").length());
                scriptSource = fileData;

                if (scriptSource.startsWith("\n"))
                    scriptSource.remove(0, 1);

                scriptTitle = script;
                scriptTitle.remove(script.length() - 4, 4);

                QStandardItem *it = new QStandardItem(scriptTitle);
                it->setData(scriptSource, Qt::UserRole + 100);

                m_scriptsModel->appendRow(it);
            }
        }
    }
}
