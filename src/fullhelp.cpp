#include "fullhelp.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFileInfo>
#include <QWebEngineView>
#include <QToolBar>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

#include <QDebug>

USING_NS_LE

FullHelp::FullHelp(QWidget *parent) :
    QFrame(parent),
    m_webView(new QWebEngineView(this)),
    m_toolBar(new QToolBar(this))
{
    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->setMargin(0);
    mainLayout->setSpacing(0);
    setLayout(mainLayout);

    m_toolBar->setStyleSheet("QToolBar { margin: 2; max-height: 46px } ");
    m_toolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);

    m_homeAction = m_toolBar->addAction(QIcon(":/res/icons/home.png"), tr("Главная"));
    connect(m_homeAction, &QAction::triggered,
            [this](){
        QFileInfo fileInfo("help/contents_ru.html");
        QUrl url = QUrl::fromLocalFile(fileInfo.absoluteFilePath());
        m_webView->load(url);
    });

    m_previousPageAction = m_toolBar->addAction(QIcon(":/res/icons/left.png"), tr("Назад"));
    connect(m_previousPageAction, &QAction::triggered,
            [this](){
        m_webView->back();
    });

    m_forwardPageAction = m_toolBar->addAction(QIcon(":/res/icons/right.png"), tr("Вперед"));
    connect(m_forwardPageAction, &QAction::triggered,
            [this](){
        m_webView->forward();
    });


    QWidget *searchForm = new QWidget(this);
    QVBoxLayout *searchFormLayout = new QVBoxLayout();
    searchFormLayout->setMargin(2);
    searchFormLayout->setSpacing(0);
    searchForm->setLayout(searchFormLayout);

    QLabel *searchTitle = new QLabel(tr("Поиск: "), searchForm);
    searchFormLayout->addWidget(searchTitle);

    QHBoxLayout *searchControlsLayout = new QHBoxLayout();
    searchControlsLayout->setSpacing(2);

    QLineEdit *searchText = new QLineEdit(searchForm);
    searchControlsLayout->addWidget(searchText, 1);

    m_prevSearchButton = new QPushButton(searchForm);
    m_prevSearchButton->setIcon(QIcon(":/res/icons/left.png"));

    connect(m_prevSearchButton, &QPushButton::clicked,
            [this, searchText](){
        if (!searchText->text().isEmpty())
            m_webView->findText(searchText->text(), QWebEnginePage::FindBackward);
    });

    searchControlsLayout->addWidget(m_prevSearchButton);

    m_nextSearchButton = new QPushButton(searchForm);
    m_nextSearchButton->setIcon(QIcon(":/res/icons/right.png"));

    connect(m_nextSearchButton, &QPushButton::clicked,
            [this,searchText](){
        if (!searchText->text().isEmpty())
            m_webView->findText(searchText->text());
    });

    searchControlsLayout->addWidget(m_nextSearchButton);

    searchFormLayout->addLayout(searchControlsLayout);

    m_toolBar->addWidget(searchForm);

    mainLayout->addWidget(m_toolBar, 0);

    QFrame *sp = new QFrame(this);
    sp->setFrameShape(QFrame::StyledPanel);
    sp->setFixedHeight(1);
    mainLayout->addWidget(sp, 1);

    mainLayout->addWidget(m_webView, 1);

    QFileInfo fileInfo("help/contents_ru.html");
    QUrl url = QUrl::fromLocalFile(fileInfo.absoluteFilePath());
    m_webView->load(url);

    setFrameShape(QFrame::StyledPanel);
}
