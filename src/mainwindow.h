#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "le_global.h"
#include "sandbox.h"

#include <QWidget>

class QToolBar;
class QTabWidget;
class QSplitter;

NS_BEGIN_LE

class CodeEdit;
class OutputWidget;
class ErrorsOutputWidget;
class FullHelp;

class MainWindow : public QWidget
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);

    void openFile(const QString &filePath);

private:
    void initToolbarActions();
    void initCodeEditorAutoCompleter();

private:
    QString m_currentFilePath;

    QToolBar *m_toolBar;
    QAction *m_newFileAction;
    QAction *m_openFileAction;
    QAction *m_saveFileAction;
    QAction *m_appendAction;
    QAction *m_editorAction;
    QAction *m_helpAction;
    QAction *m_runSanboxAction;

    QSplitter *m_editorSplitter;
    CodeEdit *m_codeEdit;
    QTabWidget *m_infoTabs;
    OutputWidget *m_outputWidget;
    ErrorsOutputWidget *m_errorsOutputWidget;
    FullHelp *m_fullHelp;

    Sandbox m_sandbox;
};

NS_END_LE

#endif // MAINWINDOW_H
