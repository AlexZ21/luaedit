#ifndef FULLHELPPOPUP_H
#define FULLHELPPOPUP_H

#include "le_global.h"

#include <QFrame>

class QLineEdit;
class QPushButton;

NS_BEGIN_LE

class CodeEdit;

class FullHelpPopup : public QFrame
{
    Q_OBJECT
public:
    explicit FullHelpPopup(QWidget *parent = nullptr);

    void show(const QPoint &point);

private:
    QLineEdit *m_searchLineEdit;
    QPushButton *m_nextButton;
    QPushButton *m_prevButton;

    CodeEdit *m_helpArea;
};

NS_END_LE

#endif // FULLHELPPOPUP_H
