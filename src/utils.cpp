#include "utils.h"

#include <QWidget>
#include <QMessageBox>
#include <QIcon>
#include <QFile>
#include <QFileInfo>

void le::showWarningMessageBox(const QString &title, const QString &text, QWidget *parent)
{
    QMessageBox *msgBox = new QMessageBox(parent);
    msgBox->setIconPixmap(QIcon(":/res/icons/error.png").pixmap(64, 64));
    msgBox->setWindowTitle(title);
    msgBox->setText(text);
    msgBox->exec();
}

void le::saveScript(const QString &fileName, const QString &code)
{
    QFileInfo fileInfo(fileName);
    QFile file(fileName);
    if (file.open(QIODevice::WriteOnly)) {
        file.write(QString("[title]\n").toUtf8());
        file.write(QString(fileInfo.baseName() + "\n").toUtf8());
        file.write(QString("[source]\n").toUtf8());
        file.write(QString(code + "\n").toUtf8());
    }
}
