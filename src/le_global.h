#ifndef LE_GLOBAL_H
#define LE_GLOBAL_H

#define NS_BEGIN_LE namespace le {
#define NS_END_LE }
#define USING_NS_LE using namespace le;

#endif // LE_GLOBAL_H
