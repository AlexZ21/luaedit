#ifndef FULLHELP_H
#define FULLHELP_H

#include "le_global.h"

#include <QFrame>

class QWebEngineView;
class QToolBar;
class QAction;
class QPushButton;

NS_BEGIN_LE

class FullHelp : public QFrame
{
    Q_OBJECT
public:
    explicit FullHelp(QWidget *parent = nullptr);

private:
    QWebEngineView *m_webView;

    QToolBar *m_toolBar;
    QAction *m_homeAction;
    QAction *m_previousPageAction;
    QAction *m_forwardPageAction;
    QPushButton *m_nextSearchButton;
    QPushButton *m_prevSearchButton;

};

NS_END_LE

#endif // FULLHELP_H
