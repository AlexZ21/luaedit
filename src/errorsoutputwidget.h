#ifndef ERRORSOUTPUTWIDGET_H
#define ERRORSOUTPUTWIDGET_H

#include "le_global.h"

#include <QWidget>

class QTableView;
class QStandardItemModel;

NS_BEGIN_LE

class ErrorsOutputWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ErrorsOutputWidget(QWidget *parent = nullptr);

    void appendError(const QString &text);
    void clear();

private:
    void resetColumns();

signals:
    void jumpToLine(int line);

private:
    QTableView *m_errorsTableView;
    QStandardItemModel *m_errorsModel;
};

NS_END_LE

#endif // ERRORSOUTPUTWIDGET_H
