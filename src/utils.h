#ifndef UTILS_H
#define UTILS_H

#include "le_global.h"

#include <QString>

class QWidget;

NS_BEGIN_LE

void showWarningMessageBox(const QString &title,
                           const QString &text,
                           QWidget *parent = nullptr);

void saveScript(const QString &fileName, const QString &code);

NS_END_LE

#endif // UTILS_H
