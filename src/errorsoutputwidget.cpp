#include "errorsoutputwidget.h"

#include <QTableView>
#include <QStandardItemModel>
#include <QVBoxLayout>
#include <QHeaderView>

USING_NS_LE

ErrorsOutputWidget::ErrorsOutputWidget(QWidget *parent) :
    QWidget(parent),
    m_errorsTableView(new QTableView(this)),
    m_errorsModel(new QStandardItemModel())
{
    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->setMargin(0);
    setLayout(mainLayout);

    resetColumns();
    m_errorsTableView->setModel(m_errorsModel);
    m_errorsTableView->setFrameShape(QFrame::NoFrame);
    m_errorsTableView->horizontalHeader()->setStretchLastSection(true);
    m_errorsTableView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    connect(m_errorsTableView, &QTableView::doubleClicked,
            [this](const QModelIndex &index){
        int row = index.row();
        QModelIndex lineNumIndex = m_errorsModel->index(row, 0);
        if (lineNumIndex.isValid()) {
            int line = lineNumIndex.data().toInt();
            emit jumpToLine(line);
        }
    });

    mainLayout->addWidget(m_errorsTableView);
}

void ErrorsOutputWidget::appendError(const QString &text)
{
    QString errText = text;
    errText.remove("lua: error: ");
    QStringList errTextParts = errText.split(":");

    if (errTextParts.size() == 1) {
        if (errTextParts.at(0) == "stopped_by_user")
            return;
        m_errorsModel->appendRow({new QStandardItem(), new QStandardItem(errTextParts.at(0))});
    } else {
        m_errorsModel->appendRow({new QStandardItem(errTextParts.at(1)), new QStandardItem(errTextParts.at(2))});
    }

}

void ErrorsOutputWidget::clear()
{
    m_errorsModel->clear();
    resetColumns();
}

void ErrorsOutputWidget::resetColumns()
{
    m_errorsModel->setHorizontalHeaderLabels({tr("Строка"), tr("Описание")});
}
