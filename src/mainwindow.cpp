#include "mainwindow.h"
#include "codeedit.h"
#include "outputwidget.h"
#include "errorsoutputwidget.h"
#include "appendpopup.h"
#include "fullhelppopup.h"
#include "fullhelp.h"
#include "utils.h"

#include <QHBoxLayout>
#include <QToolBar>
#include <QSplitter>
#include <QAction>
#include <QIcon>
#include <QCompleter>
#include <QStringListModel>
#include <QTableWidget>
#include <QFile>
#include <QFileDialog>
#include <QFileInfo>
#include <QMenu>
#include <QButtonGroup>

#include <QDebug>

USING_NS_LE

MainWindow::MainWindow(QWidget *parent) :
    QWidget(parent),
    m_toolBar(new QToolBar(this)),
    m_codeEdit(new CodeEdit(this)),
    m_infoTabs(new QTabWidget(this)),
    m_outputWidget(new OutputWidget(this)),
    m_errorsOutputWidget(new ErrorsOutputWidget(this)),
    m_fullHelp(new FullHelp(this))
{
    QHBoxLayout *mainLayout = new QHBoxLayout();
    mainLayout->setMargin(2);
    mainLayout->setSpacing(2);
    setLayout(mainLayout);

    // Toolbar
    m_toolBar->setOrientation(Qt::Vertical);
    m_toolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    m_toolBar->setStyleSheet("QToolBar { margin: 0; } "
                             "QToolButton { height: 38px; width: 56px; } ");
    initToolbarActions();
    mainLayout->addWidget(m_toolBar, 0, Qt::AlignLeft);

    // Разделитель экрана
    m_editorSplitter = new QSplitter(this);
    m_editorSplitter->setOrientation(Qt::Vertical);
    mainLayout->addWidget(m_editorSplitter, 1);

    // Рекдактор кода
    m_codeEdit->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(m_codeEdit, &CodeEdit::customContextMenuRequested,
            [this](const QPoint &point){
        QMenu *menu = m_codeEdit->createStandardContextMenu();
        QString selectedText = m_codeEdit->textCursor().selectedText();
        if (!selectedText.isEmpty()) {
            menu->addAction("Сохранить выделенный код для вставки",
                            [this, selectedText](){
                QString filePath = QFileDialog::getSaveFileName(this, tr("Сохранить файл"), "as",
                                                                "Lua (*.lua)");
                if (!filePath.isEmpty()) {
                    saveScript(filePath, selectedText);
                }
            });
        }
        menu->exec(mapToGlobal(point));
    });

    initCodeEditorAutoCompleter();
    m_editorSplitter->addWidget(m_codeEdit);

    // Табы
    m_infoTabs->addTab(m_outputWidget, QIcon(":/res/icons/console.png"), tr("Вывод"));
    m_infoTabs->addTab(m_errorsOutputWidget, QIcon(":/res/icons/bug.png"), tr("Проблемы"));

    m_editorSplitter->addWidget(m_infoTabs);
    m_editorSplitter->setSizes({m_codeEdit->maximumHeight(), 100});

    // Песочница
    connect(&m_sandbox, &Sandbox::started,
            [this](){
        m_runSanboxAction->setIcon(QIcon(":/res/icons/stop.png"));
        m_runSanboxAction->setText(tr("Стоп"));
        m_outputWidget->clear();
        m_errorsOutputWidget->clear();
    });

    connect(&m_sandbox, &Sandbox::stopped,
            [this](){
        m_runSanboxAction->setIcon(QIcon(":/res/icons/play.png"));
        m_runSanboxAction->setText(tr("Запустить"));
    });

    m_sandbox.setOutputDevice(m_outputWidget->device());

    connect(&m_sandbox, &Sandbox::error, m_errorsOutputWidget,
            [this](const QString &text){
        m_errorsOutputWidget->appendError(text);
        m_infoTabs->setCurrentWidget(m_errorsOutputWidget);
    }, Qt::QueuedConnection);

    // Таб с ошибками
    connect(m_errorsOutputWidget, &ErrorsOutputWidget::jumpToLine,
            [this](int line){
        QTextCursor cursor(m_codeEdit->document()->findBlockByLineNumber(line - 1));
        m_codeEdit->setTextCursor(cursor);
    });

    // Справка
    m_fullHelp->hide();
    mainLayout->addWidget(m_fullHelp, 1);

    setWindowFlags(Qt::Window);
    resize(800, 480);
}

void MainWindow::openFile(const QString &filePath)
{
    QFile file(filePath);
    if (!file.open(QIODevice::ReadOnly)) {
        showWarningMessageBox(tr("Открыть файл"),
                              tr("Ошибка открытия файла: ") + filePath, this);
        return;
    }
    m_codeEdit->setPlainText(file.readAll());
    m_currentFilePath = filePath;
    setWindowTitle("LuaEdit - " + m_currentFilePath);
}

void MainWindow::initToolbarActions()
{
    // Новый файл
    m_newFileAction = m_toolBar->addAction(QIcon(":/res/icons/new.png"), tr("Новый"));
    connect(m_newFileAction, &QAction::triggered,
            [this](){
        m_codeEdit->clear();
        m_currentFilePath.clear();
        setWindowTitle("LuaEdit");
    });

    // Открыть файл
    m_openFileAction = m_toolBar->addAction(QIcon(":/res/icons/open.png"), tr("Открыть"));
    connect(m_openFileAction, &QAction::triggered,
            [this](){
        static QString lastOpenPath = QDir::homePath();
        QString filePath = QFileDialog::getOpenFileName(this, tr("Открыть файл"), lastOpenPath,
                                                        "Lua (*.lua)");
        if (!filePath.isEmpty()) {
            openFile(filePath);
            QFileInfo fileInfo(filePath);
            lastOpenPath = fileInfo.dir().absolutePath();
        }
    });

    // Сохранить файл
    m_saveFileAction = m_toolBar->addAction(QIcon(":/res/icons/save.png"), tr("Сохранить"));
    connect(m_saveFileAction, &QAction::triggered,
            [this](){
        static QString lastSavePath = QDir::homePath();
        QString filePath = QFileDialog::getSaveFileName(this, tr("Сохранить файл"), lastSavePath,
                                                        "Lua (*.lua)");
        if (!filePath.isEmpty()) {
            QFile file(filePath);
            if (file.open(QIODevice::WriteOnly)) {
                file.write(m_codeEdit->toPlainText().toLatin1());
                QFileInfo fileInfo(filePath);
                lastSavePath = fileInfo.dir().absolutePath();
                m_currentFilePath = filePath;
                setWindowTitle("LuaEdit - " + filePath);
            }
        }
    });

    m_toolBar->addSeparator();

    // Вставка
    m_appendAction = m_toolBar->addAction(QIcon(":/res/icons/add.png"), tr("Вставить"));
    connect(m_appendAction, &QAction::triggered,
            [this](){
        QPoint p = mapToGlobal(m_toolBar->widgetForAction(m_appendAction)->pos());
        p.setX(p.x() + m_toolBar->width() + 4);
        AppendPopup *appendPopup = new AppendPopup("as");

        connect(appendPopup, &AppendPopup::selected,
                [this, appendPopup](const QString &code){
            QTextCursor cursor = m_codeEdit->textCursor();
            cursor.insertText(code);
            m_codeEdit->setTextCursor(cursor);
            appendPopup->close();
        });

        appendPopup->show(p);
    });

    m_toolBar->addSeparator();

    // Редактор
    m_editorAction = m_toolBar->addAction(QIcon(":/res/icons/editor.png"), tr("Редактор"));
    m_editorAction->setCheckable(true);
    m_editorAction->setChecked(true);
    connect(m_editorAction, &QAction::triggered,
            [this](){
        m_fullHelp->hide();
        m_editorSplitter->show();
    });

    // Справка
    m_helpAction = m_toolBar->addAction(QIcon(":/res/icons/help.png"), tr("Помощь"));
    m_helpAction->setCheckable(true);
    connect(m_helpAction, &QAction::triggered,
            [this](){
        m_editorSplitter->hide();
        m_fullHelp->show();
        //        QPoint p = mapToGlobal(m_toolBar->widgetForAction(m_helpAction)->pos());
        //        p.setX(p.x() + m_toolBar->width() + 4);
        //        FullHelpPopup *fullHelpPopup = new FullHelpPopup();
        //        fullHelpPopup->show(p);
    });

    QButtonGroup *sectionButtons = new QButtonGroup(this);
    sectionButtons->addButton(reinterpret_cast<QAbstractButton*>(m_toolBar->widgetForAction(m_editorAction)));
    sectionButtons->addButton(reinterpret_cast<QAbstractButton*>(m_toolBar->widgetForAction(m_helpAction)));

    QWidget* empty = new QWidget(m_toolBar);
    empty->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    m_toolBar->addWidget(empty);

    m_toolBar->addSeparator();

    // Запустить скрипт
    m_runSanboxAction = m_toolBar->addAction(QIcon(":/res/icons/play.png"), tr("Запустить"));
    connect(m_runSanboxAction, &QAction::triggered,
            [this](){
        if (!m_sandbox.isRunning())
            m_sandbox.start(m_codeEdit->toPlainText());
        else
            m_sandbox.stop();
    });

}

void MainWindow::initCodeEditorAutoCompleter()
{
    QFile keywordsFile(":/res/lua_content/autocomplete_keywords.txt");
    if (keywordsFile.exists()) {

        QCompleter *completer = new QCompleter(m_codeEdit);
        completer->setModelSorting(QCompleter::CaseInsensitivelySortedModel);
        completer->setCaseSensitivity(Qt::CaseInsensitive);
        completer->setWrapAround(false);

        QStringList keywords;
        if (keywordsFile.open(QIODevice::ReadOnly)) {
            while (!keywordsFile.atEnd()) {
                QByteArray line = keywordsFile.readLine();
                if (!line.isEmpty())
                    keywords << line.trimmed();
            }
        }

        completer->setModel(new QStringListModel(keywords, completer));
        m_codeEdit->setCompleter(completer);
    }
}
