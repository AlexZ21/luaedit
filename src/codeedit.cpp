#include "codeedit.h"

#include <QCompleter>
#include <QPainter>
#include <QAbstractItemModel>
#include <QStringListModel>
#include <QAbstractItemView>
#include <QScrollBar>
#include <QVBoxLayout>
#include <QFont>

USING_NS_LE

CodeEdit::CodeEdit(QWidget *parent) :
    QPlainTextEdit(parent),
    m_c(nullptr)
{
    m_lineNumberArea = new CodeEditLineNumberArea(this);

    connect(this, &CodeEdit::blockCountChanged,
            this, &CodeEdit::updateLineNumberAreaWidth);
    connect(this, &CodeEdit::updateRequest,
            this, &CodeEdit::updateLineNumberArea);
    connect(this, &CodeEdit::cursorPositionChanged,
            this, &CodeEdit::highlightCurrentLine);

    updateLineNumberAreaWidth(0);
    highlightCurrentLine();

    const int tabStop = 4;  // 4 characters

    QString spaces;
    for (int i = 0; i < tabStop; ++i)
        spaces += " ";

    QFontMetrics metrics(font());
    setTabStopWidth(metrics.width(spaces));

    m_highlighter = new CodeHighlighter(document());

    QFont f = font();
    f.setFamily("Source Code Pro");
    f.setPixelSize(12);
    setFont(f);
}

CodeEdit::~CodeEdit()
{

}

void CodeEdit::lineNumberAreaPaintEvent(QPaintEvent *event)
{
    QPainter p(m_lineNumberArea);
    p.fillRect(event->rect(), QColor("#0A000000"));

    QTextBlock block = firstVisibleBlock();
    int blockNumber = block.blockNumber();
    int top = (int) blockBoundingGeometry(block).translated(contentOffset()).top();
    int bottom = top + (int) blockBoundingRect(block).height();

    while (block.isValid() && top <= event->rect().bottom()) {
        if (block.isVisible() && bottom >= event->rect().top()) {
            QString number = QString::number(blockNumber + 1);
            p.setPen(Qt::black);
            p.drawText(0, top, m_lineNumberArea->width() - 3, fontMetrics().height(),
                       Qt::AlignRight, number);
        }

        block = block.next();
        top = bottom;
        bottom = top + (int)blockBoundingRect(block).height();
        ++blockNumber;
    }
}

int CodeEdit::lineNumberAreaWidth()
{
    int digits = 1;
    int max = qMax(1, blockCount());
    while (max >= 10) {
        max /= 10;
        ++digits;
    }
    int space = 10 + fontMetrics().width(QLatin1Char('9')) * digits;
    return space;
}

void CodeEdit::setCompleter(QCompleter *c)
{
    if (m_c) {
        QObject::disconnect(m_c, 0, this, 0);
        delete m_c;
    }

    m_c = c;

    if (!m_c)
        return;

    m_c->setWidget(this);
    m_c->setCompletionMode(QCompleter::PopupCompletion);
    m_c->setCaseSensitivity(Qt::CaseInsensitive);
    QObject::connect(m_c, static_cast<void (QCompleter::*)(const QString &)>(&QCompleter::activated),
                     this, &CodeEdit::insertCompletion);
}

QCompleter *CodeEdit::completer() const
{
    return m_c;
}

void CodeEdit::resizeEvent(QResizeEvent *event)
{
    QPlainTextEdit::resizeEvent(event);

    QRect cr = contentsRect();
    m_lineNumberArea->setGeometry(QRect(cr.left(), cr.top(), lineNumberAreaWidth(), cr.height()));
}

void CodeEdit::keyPressEvent(QKeyEvent *event)
{
//    if (event->key() == Qt::Key_F1) {
//        QString currentText = blockUnderCursor();

//        QRect cr = cursorRect();
//        QPoint p = cr.topLeft();
//        p.setY(p.y() + cr.height());
//        showHelpFor(currentText, mapToGlobal(p));

//        return;
//    }

    if (m_c && m_c->popup()->isVisible()) {
        switch (event->key()) {
        case Qt::Key_Enter:
        case Qt::Key_Return:
        case Qt::Key_Escape:
        case Qt::Key_Tab:
        case Qt::Key_Backtab:
            event->ignore();
            return;
        default:
            break;
        }
    }

    bool isShortcut = ((event->modifiers() & Qt::ControlModifier) && event->key() == Qt::Key_E); // CTRL+E
    if (!m_c || !isShortcut)
        QPlainTextEdit::keyPressEvent(event);

    const bool ctrlOrShift = event->modifiers() & (Qt::ControlModifier | Qt::ShiftModifier);
    if (!m_c || (ctrlOrShift && event->text().isEmpty()))
        return;

    static QString eow("~!@#$%^&*()_+{}|:\"<>?,./;'[]\\-=");
    bool hasModifier = (event->modifiers() != Qt::NoModifier) && !ctrlOrShift;
    QString completionPrefix = textUnderCursor();

    if (!isShortcut && (hasModifier || event->text().isEmpty()|| completionPrefix.length() < 2 || eow.contains(event->text().right(1)))) {
        m_c->popup()->hide();
        return;
    }

    if (completionPrefix != m_c->completionPrefix()) {
        m_c->setCompletionPrefix(completionPrefix);
        m_c->popup()->setCurrentIndex(m_c->completionModel()->index(0, 0));
    }

    QRect cr = cursorRect();
    cr.setWidth(m_c->popup()->sizeHintForColumn(0) + m_c->popup()->verticalScrollBar()->sizeHint().width());

    m_c->complete(cr);
}

void CodeEdit::focusInEvent(QFocusEvent *event)
{
    if (m_c)
        m_c->setWidget(this);
    QPlainTextEdit::focusInEvent(event);
}

QString CodeEdit::textUnderCursor() const
{
    QTextCursor tc = textCursor();
    tc.select(QTextCursor::WordUnderCursor);
    return tc.selectedText();
}

QString CodeEdit::blockUnderCursor() const
{
    QTextCursor tc = textCursor();

    int i = tc.positionInBlock();
    int j = tc.positionInBlock();

    tc.select(QTextCursor::BlockUnderCursor);
    QString selectedtext = tc.selectedText();

    while (i > 1) {
        if (selectedtext.at(i) == ' ' || selectedtext.at(i) == '\t')
            break;
        --i;
    }

    while (j < selectedtext.size()) {
        if (selectedtext.at(j) == ' ' || selectedtext.at(j) == '\t' || selectedtext.at(j) == '\n')
            break;
        ++j;
    }

    return selectedtext.mid(i, j - i);
}

void CodeEdit::updateLineNumberAreaWidth(int newBlockCount)
{
    Q_UNUSED(newBlockCount);
    setViewportMargins(lineNumberAreaWidth(), 0, 0, 0);
}

void CodeEdit::highlightCurrentLine()
{
    QList<QTextEdit::ExtraSelection> extraSelections;

    if (!isReadOnly()) {
        QTextEdit::ExtraSelection selection;
        QColor lineColor = QColor(Qt::yellow).lighter(160);
        selection.format.setBackground(lineColor);
        selection.format.setProperty(QTextFormat::FullWidthSelection, true);
        selection.cursor = textCursor();
        selection.cursor.clearSelection();
        extraSelections.append(selection);
    }

    setExtraSelections(extraSelections);
}

void CodeEdit::updateLineNumberArea(const QRect &rect, int dy)
{
    if (dy)
        m_lineNumberArea->scroll(0, dy);
    else
        m_lineNumberArea->update(0, rect.y(), m_lineNumberArea->width(), rect.height());

    if (rect.contains(viewport()->rect()))
        updateLineNumberAreaWidth(0);
}

void CodeEdit::insertCompletion(const QString &completion)
{
    if (m_c->widget() != this)
        return;
    QTextCursor tc = textCursor();
    int extra = completion.length() - m_c->completionPrefix().length();
    tc.movePosition(QTextCursor::Left);
    tc.movePosition(QTextCursor::EndOfWord);
    tc.insertText(completion.right(extra));
    setTextCursor(tc);
}

CodeHighlighter::CodeHighlighter(QTextDocument *parent) :
    QSyntaxHighlighter(parent)
{
    HighlightingRule rule;

    m_keywordFormat.setForeground(Qt::darkBlue);
    m_keywordFormat.setFontWeight(QFont::Bold);

    QStringList keywordPatterns;
    keywordPatterns << "\\band\\b"
                    << "\\bbreak\\b"
                    << "\\bdo\\b"
                    << "\\belse\\b"
                    << "\\belseif\\b"
                    << "\\bend\\b"
                    << "\\bfalse\\b"
                    << "\\bfor\\b"
                    << "\\bfunction\\b"
                    << "\\bif\\b"
                    << "\\bin\\b"
                    << "\\blocal\\b"
                    << "\\bnil\\b"
                    << "\\bnot\\b"
                    << "\\bor\\b"
                    << "\\brepeat\\b"
                    << "\\breturn\\b"
                    << "\\bthen\\b"
                    << "\\btrue\\b"
                    << "\\buntil\\b"
                    << "\\bwhile\\b";

    foreach (const QString &pattern, keywordPatterns) {
        rule.pattern = QRegularExpression(pattern);
        rule.format = m_keywordFormat;
        m_highlightingRules.append(rule);
    }

    m_quotationFormat.setForeground(Qt::darkGreen);
    rule.pattern = QRegularExpression("\".*\"");
    rule.format = m_quotationFormat;
    m_highlightingRules.append(rule);

    //    _functionFormat.setFontItalic(true);
    m_functionFormat.setForeground(Qt::blue);
    rule.pattern = QRegularExpression("\\b[A-Za-z0-9_]+(?=\\()");
    rule.format = m_functionFormat;
    m_highlightingRules.append(rule);

    m_singleLineCommentFormat.setForeground(Qt::gray);
    rule.pattern = QRegularExpression("--[^\n]*");
    rule.format = m_singleLineCommentFormat;
    m_highlightingRules.append(rule);

}

void CodeHighlighter::highlightBlock(const QString &text)
{
    foreach (const HighlightingRule &rule, m_highlightingRules) {
        QRegularExpressionMatchIterator matchIterator = rule.pattern.globalMatch(text);
        while (matchIterator.hasNext()) {
            QRegularExpressionMatch match = matchIterator.next();
            setFormat(match.capturedStart(), match.capturedLength(), rule.format);
        }
    }
}
