#include "fullhelppopup.h"
#include "codeedit.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QFile>

USING_NS_LE

FullHelpPopup::FullHelpPopup(QWidget *parent) :
    QFrame(parent),
    m_searchLineEdit(new QLineEdit(this)),
    m_nextButton(new QPushButton(this)),
    m_prevButton(new QPushButton(this)),
    m_helpArea(new CodeEdit(this))
{
    resize(600, 300);

    static QString searchText;
    static int cursorPos;

    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->setMargin(2);
    mainLayout->setSpacing(2);
    setLayout(mainLayout);

    QHBoxLayout *headerLayout = new QHBoxLayout();
    headerLayout->setSpacing(2);
    mainLayout->addLayout(headerLayout);

    m_searchLineEdit->setText(searchText);
    headerLayout->addWidget(m_searchLineEdit, 1);

    m_prevButton->setIcon(QIcon(":/res/icons/left.png"));
    headerLayout->addWidget(m_prevButton);

    m_nextButton->setIcon(QIcon(":/res/icons/right.png"));
    headerLayout->addWidget(m_nextButton);

    QFile helpFile(":/res/lua_content/funcs_help3.txt");
    if (helpFile.open(QIODevice::ReadOnly)) {
        QString helpStr = helpFile.readAll();
        helpStr.replace("!--------\r\n", QString());
        m_helpArea->setPlainText(helpStr);
    }
    if (cursorPos != 0) {
        QTextCursor cursor = m_helpArea->textCursor();
        cursor.setPosition(cursorPos);
        cursor.select(QTextCursor::WordUnderCursor);
        m_helpArea->setTextCursor(cursor);
    }
    mainLayout->addWidget(m_helpArea);

    connect(m_prevButton, &QPushButton::clicked,
            [this](){
        if (!m_searchLineEdit->text().isEmpty()) {
            QTextDocument::FindFlags flag;
            flag |= QTextDocument::FindBackward;

            if (m_helpArea->find(m_searchLineEdit->text(), flag)) {
                QTextCursor cursor = m_helpArea->textCursor();
                cursor.select(QTextCursor::WordUnderCursor);

                searchText = m_searchLineEdit->text();
                cursorPos = cursor.position();
            }

        }
    });

    connect(m_nextButton, &QPushButton::clicked,
            [this](){
        if (!m_searchLineEdit->text().isEmpty()) {
            QTextDocument::FindFlags flag;

            if (m_helpArea->find(m_searchLineEdit->text(), flag)) {
                QTextCursor cursor = m_helpArea->textCursor();
                cursor.select(QTextCursor::WordUnderCursor);

                searchText = m_searchLineEdit->text();
                cursorPos = cursor.position();
            }

        }
    });

    setFrameShape(QFrame::StyledPanel);
    setAttribute(Qt::WA_DeleteOnClose, true);
    setWindowFlags(Qt::Popup);
}

void FullHelpPopup::show(const QPoint &point)
{
    move(point);
    QWidget::show();
}
