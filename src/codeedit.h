#ifndef CODEEDIT_H
#define CODEEDIT_H

#include "le_global.h"

#include <QPlainTextEdit>
#include <QSyntaxHighlighter>
#include <QRegularExpression>
#include <QAbstractItemModel>

class QCompleter;

NS_BEGIN_LE

//! Редактор кода
class CodeEdit : public QPlainTextEdit
{
    Q_OBJECT
public:
    explicit CodeEdit(QWidget *parent = 0);
    ~CodeEdit();

    void lineNumberAreaPaintEvent(QPaintEvent *event);
    int lineNumberAreaWidth();

    void setCompleter(QCompleter *c);
    QCompleter *completer() const;

protected:
    void resizeEvent(QResizeEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void focusInEvent(QFocusEvent *event);

private:
    QString textUnderCursor() const;
    QString blockUnderCursor() const;

private slots:
    void updateLineNumberAreaWidth(int newBlockCount);
    void highlightCurrentLine();
    void updateLineNumberArea(const QRect &rect, int dy);
    void insertCompletion(const QString &completion);

private:
    QWidget *m_lineNumberArea;
    QSyntaxHighlighter *m_highlighter;
    QCompleter *m_c;
};

//! Номер линии
class CodeEditLineNumberArea : public QWidget
{
public:
    CodeEditLineNumberArea(CodeEdit *editor) : QWidget(editor) {
        m_codeEdit = editor;
    }

    QSize sizeHint() const {
        return QSize(m_codeEdit->lineNumberAreaWidth(), 0);
    }

protected:
    void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE {
        m_codeEdit->lineNumberAreaPaintEvent(event);
    }

private:
    CodeEdit *m_codeEdit;
};

//! Подсветка синтаксиса
class CodeHighlighter : public QSyntaxHighlighter
{
    Q_OBJECT
public:
    CodeHighlighter(QTextDocument *parent = 0);

protected:
    void highlightBlock(const QString &text);

private:
    struct HighlightingRule {
        QRegularExpression pattern;
        QTextCharFormat format;
    };

private:
    QVector<HighlightingRule> m_highlightingRules;

    QTextCharFormat m_keywordFormat;
    QTextCharFormat m_singleLineCommentFormat;
    QTextCharFormat m_quotationFormat;
    QTextCharFormat m_functionFormat;
};

NS_END_LE

#endif // CODEEDIT_H
