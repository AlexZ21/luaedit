#include "sandbox.h"

#include <QIODevice>
#include <QThread>

#include <QDebug>

USING_NS_LE

bool Sandbox::m_forceSandboxStop = false;

Sandbox::Sandbox(QObject *parent) :
    QObject(parent),
    m_running(false),
    m_outputDevice(nullptr)
{
    m_stoppedTimer.setInterval(200);
    m_stoppedTimer.setSingleShot(true);
    connect(&m_stoppedTimer, &QTimer::timeout,
            [this](){
        stop();
    });

    connect(this, &Sandbox::runStoppedTimer, this,
            [this](){
        m_stoppedTimer.start();
    }, Qt::QueuedConnection);
}

Sandbox::~Sandbox()
{
    stop();
}

void Sandbox::start(const QString &script)
{
    if (m_running)
        return;

    m_running = true;
    emit started();

    m_luaState = new old_sol::state();
    m_luaState->open_libraries();

    lua_setoutcallback(m_luaState->lua_state(), &Sandbox::luaOutputCallback, this);
    lua_sethook(m_luaState->lua_state(), &Sandbox::sandboxLineHookFunc, LUA_MASKLINE, 0);

    m_thread = std::thread([this, script](){
        try {
            m_luaState->script(script.toStdString());
        } catch (const std::exception &ex) {
            emit error(ex.what());
        }

        emit runStoppedTimer();
    });
}

void Sandbox::stop()
{
    if (m_stoppedTimer.isActive())
        m_stoppedTimer.stop();

    if (!m_running)
        return;

    m_forceSandboxStop = true;

    if (m_thread.joinable())
        m_thread.join();

    delete m_luaState;

    m_forceSandboxStop = false;

    m_running = false;
    emit stopped();
}

bool Sandbox::isRunning() const
{
    return m_running;
}

void Sandbox::setOutputDevice(QIODevice *outputDevice)
{
    m_outputDevice = outputDevice;
}

void Sandbox::writeOutput(const QString &text)
{
    if (m_outputDevice)
        m_outputDevice->write(text.toUtf8() + "\n");
}

void Sandbox::sandboxLineHookFunc(lua_State *L, lua_Debug *ar)
{
    if(ar->event == LUA_HOOKLINE) {
        if(m_forceSandboxStop) {
            luaL_error(L, "stopped_by_user");
            m_forceSandboxStop = false;
        }
    }
}

void Sandbox::luaOutputCallback(char *s, int lenght, void *userdata)
{
    reinterpret_cast<Sandbox*>(userdata)->writeOutput(QByteArray(s, lenght));
}

