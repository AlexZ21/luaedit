#include "outputwidget.h"

#include <QDebug>

#include <QVBoxLayout>
#include <QPlainTextEdit>

USING_NS_LE

ThreadSafeBuffer::ThreadSafeBuffer(QObject *parent) :
    QBuffer(parent)
{

}

void ThreadSafeBuffer::globalLock()
{
    m_mutex.lock();
}

void ThreadSafeBuffer::globalUnlock()
{
    m_mutex.unlock();
}

qint64 ThreadSafeBuffer::readData(char *data, qint64 maxlen)
{
//    QMutexLocker locker(&m_mutex);
    return QBuffer::readData(data, maxlen);
}

qint64 ThreadSafeBuffer::readLineData(char *data, qint64 maxlen)
{
//    QMutexLocker locker(&m_mutex);
    return QBuffer::readLineData(data, maxlen);
}

qint64 ThreadSafeBuffer::writeData(const char *data, qint64 len)
{
    QMutexLocker locker(&m_mutex);
    return QBuffer::writeData(data, len);
}

OutputWidget::OutputWidget(QWidget *parent) :
    QWidget(parent),
    m_outputArea(new QPlainTextEdit(this))
{
    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->setMargin(0);
    setLayout(mainLayout);

    m_outputArea->setFrameShape(QFrame::NoFrame);
    m_outputArea->setMaximumBlockCount(1000);
    mainLayout->addWidget(m_outputArea);

    m_outputBufferDevice.open(QIODevice::ReadWrite);

    m_readOutputBufferTimer.setInterval(50);
    connect(&m_readOutputBufferTimer, &QTimer::timeout,
            [this](){
        if (m_outputBufferDevice.size() > 0) {
            m_outputBufferDevice.globalLock();
            m_outputBufferDevice.seek(0);
            m_outputArea->appendPlainText(QString(m_outputBufferDevice.readAll()).toUtf8());
            m_outputBufferDevice.buffer().clear();
            m_outputBufferDevice.seek(0);
            m_outputBufferDevice.globalUnlock();
        }
    });
    m_readOutputBufferTimer.start();
}

QIODevice *OutputWidget::device()
{
    return &m_outputBufferDevice;
}

void OutputWidget::clear()
{
    m_outputArea->clear();
}


