#ifndef OUTPUTWIDGET_H
#define OUTPUTWIDGET_H

#include "le_global.h"

#include <QWidget>
#include <QBuffer>
#include <QTimer>
#include <QPlainTextEdit>
#include <QMutex>

class QIODevice;

NS_BEGIN_LE

class ThreadSafeBuffer : public QBuffer
{
    Q_OBJECT
public:
    explicit ThreadSafeBuffer(QObject *parent = nullptr);

    void globalLock();
    void globalUnlock();

protected:
    qint64 readData(char *data, qint64 maxlen);
    qint64 readLineData(char *data, qint64 maxlen);
    qint64 writeData(const char *data, qint64 len);

private:
    QMutex m_mutex;
};

class OutputWidget : public QWidget
{
    Q_OBJECT
public:
    explicit OutputWidget(QWidget *parent = nullptr);

    QIODevice *device();

    void clear();

private:
    ThreadSafeBuffer m_outputBufferDevice;
    QTimer m_readOutputBufferTimer;

    QPlainTextEdit *m_outputArea;

};

NS_END_LE

#endif // OUTPUTWIDGET_H
