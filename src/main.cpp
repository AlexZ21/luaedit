#include <QApplication>
#include <QStyleFactory>
#include <QFontDatabase>

#include "mainwindow.h"



int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QApplication::setStyle(QStyleFactory::create("Fusion"));

    QFontDatabase::addApplicationFont(":/res/fonts/SourceCodePro-Regular.ttf");

    le::MainWindow mw;
    mw.show();
    if (argc > 1)
        mw.openFile(argv[1]);

    return a.exec();
}
