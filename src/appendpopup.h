#ifndef APPENPDOPUP_H
#define APPENPDOPUP_H

#include "le_global.h"

#include <QFrame>

class QListView;
class QLineEdit;
class QSortFilterProxyModel;
class QStandardItemModel;

NS_BEGIN_LE

class CodeEdit;

class AppendPopup : public QFrame
{
    Q_OBJECT
public:
    explicit AppendPopup(const QString &scriptsDirPath, QWidget *parent = nullptr);

    void show(const QPoint &point);

private:
    void loadScriptsList();

signals:
    void selected(const QString &code);

private:
    QString m_scriptsDirPath;

    QStandardItemModel *m_scriptsModel;
    QSortFilterProxyModel *m_scriptsFilterModel;
    QLineEdit *m_scriptsListFilter;
    QListView *m_scriptsListView;

    CodeEdit *m_codePreview;
};

NS_END_LE

#endif // APPENPDOPUP_H
