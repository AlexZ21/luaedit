#ifndef SANDBOX_H
#define SANDBOX_H

#include "le_global.h"
#include <old_sol.hpp>

#include <QObject>
#include <QTimer>
#include <thread>

class QIODevice;

NS_BEGIN_LE

class Sandbox : public QObject
{
    Q_OBJECT
public:
    Sandbox(QObject *parent = nullptr);
    ~Sandbox();

    void start(const QString &script);
    void stop();

    bool isRunning() const;

    void setOutputDevice(QIODevice *outputDevice);

private:
    void writeOutput(const QString &text);

    static void sandboxLineHookFunc(lua_State *L, lua_Debug *ar);
    static void luaOutputCallback(char *s, int lenght, void *userdata);

signals:
    void started();
    void stopped();
    void error(const QString &text);

    void runStoppedTimer();

private:
    std::thread m_thread;
    bool m_running;
    QTimer m_stoppedTimer;

    old_sol::state *m_luaState;

    QIODevice *m_outputDevice;

    static bool m_forceSandboxStop;

};

NS_END_LE

#endif // SANDBOX_H
